/**
 * 
 */

/**

RequireNotGuest
Renders the children if the user is logged and has a non guest account
Otherwise renders the login controller
If no user object has been found (usually because it has not yet been loaded off of the server yet) => do not render anything [TODO: perhaps render a loading indicator ???]

**/

RequireNotGuest = React.createClass({

	/** Component Properties **/

	propTypes: {
		user: React.PropTypes.object, // the user object to run checks on, if not provided we load it in using meteor
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin,
		ReactMeteor.Mixin
	],

	/** Statics **/

	statics: {

		/** Meteor **/

		meteorPropDeps: ['user'],

		getMeteorState: function (props, state) {
			return {
				user: props.user ? props.user : Meteor.user()
			}
		},

	},

	/** Render **/

	render: function () {
		if (!this.state.user) {
			return null;
		} else if (this.state.user.isGuest()) {
			return <Login onLogin={this.onLogin} />
		} else {
			return this.props.children;
		}
	},

	/** Events **/

	onLogin: function () {
		// we implement this to stop Login redirecting to the homepage 
		// but we don't actually have anything much to do here....

		if (this.props.onLogin) this.props.onLogin();
	},

});
