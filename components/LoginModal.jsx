/**
 * 
 */

LoginModal = React.createClass({

	/** Component Properties **/

	propTypes: {
		// all props passed to LoginForm component

		action: React.PropTypes.string, // the action for which the LoginModal is being launched to require
	},
	getDefaultProps: function () {
		return {
			defaultMode: 'signup',

		}
	},

	/** State **/

	getInitialState: function () {
		return {
			mode: this.props.defaultMode, // or 'signup' => show a signup form instead of the email login form
		}
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** React **/

	/** Render **/

	render: function () {
		var title;
		if (this.state.mode === 'signup') {
			title = 'Sign up';
		} else if (this.state.mode === 'login') {
			title = 'Login';
		} else if (this.state.mode === 'forgot') {
			title = 'Recover your password';
		}

		if (this.props.action) title += ' to ' + this.props.action;

		return (
			<ModalContentBox onCancel={this.props.onCancel} title={title} _delegateRender={false}>
				<LoginForm {...this.props} onModeChange={this.loginForm_modeChanged} />
			</ModalContentBox>
		);
	},

	/** UI Events **/

	loginForm_modeChanged: function (_mode) {
		this.setState({
			mode: _mode
		});
	},

});
