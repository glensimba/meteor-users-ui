/**
 * 
 */

/**

Login Page
A controller which we can redirect users to require a login before proceeding
We expose this controller publically because it's possible we want to render this controller within another controller to require login to a section of the site without redirecting
=> hence also defining onLogin

**/

Login = React.createClass({

	/** Component Properties **/

	propTypes: {
		continue: React.PropTypes.string,
		//requireLogin: React.PropTypes.bool, // might be a 1 vs an actual bool so we don't set a propType for  this

		onLogin: React.PropTypes.func, // an optional callback to invoke when we login - only ever going to be used when this controller is embedded in another controller
	},

	/** Mixins **/

	mixins: [
		React.addons.PureRenderMixin
	],

	/** State **/

	getInitialState: function () {
		return {
			
		}
	},

	/** Dimensions **/

	refreshDimensions: function () {
		var $window = jQuery(window);

		this.setState({
			windowHeight: $window.height(),
			windowWidth: $window.width()
		});
	},

	/** React **/

	componentDidMount: function () {
		
		/** Resize **/

		this.refreshDimensions();
		window.addEventListener('resize', this.refreshDimensions);

	},
	componentWillUnmount: function () {

		/** Resize **/

		window.removeEventListener('resize', this.refreshDimensions);

	},

	/** Render **/

	render: function () {
		return (
			<div className="container">

				<Header fixedWidth={true} fixed={false} transparent={true}>
					<Header_Content_LogoLeft />
				</Header>

				<Splash main={true} innerBackgroundColor="#082551" valign="middle" minHeight={this.state.windowHeight} style={{marginTop: -42}}>
					
					<div className="inner-container fixed-width">
						<div className="content-pad">
							<div style={{maxWidth: 500, margin: '0 auto'}}>
								<div className="content-box content-box-decorate loginForm-container">
									<LoginForm onLogin={this.onLogin} continue={this.props.continue ? this.props.continue : HCConfig.web_url} defaultMode="login" />
								</div>
							</div>
						</div>
					</div>
					
				</Splash>

			</div>
		);
	},

	/** Callbacks **/

	onLogin: function () {
		if (this.props.onLogin) {
			this.props.onLogin();
		} else {
			window.location = this.props.continue ? this.props.continue : HCConfig.web_url;
		}
	},

});

if (typeof Router !== 'undefined') {
	Router.route({
		path: '/login',
		controllerComponent: Login
	});
}
